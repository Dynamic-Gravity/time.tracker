# time.tracker
Task Time Keeping Webapp

Made by following the online tutorial from @TheDigiCraft

Using Moment.js

Here is their repo -> https://github.com/moment/moment/

A couple things to keep in mind:

1) If on linux, the json file needs to have its permission set to 666.
  ~$ chmod 666 data.json

2) Needs php to run. This will not run on your box without it.

3) If you like this, or find it useful, pleas share! :)
