<?php include('./functions.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Time.Tracker</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
      *:disabled {
        background-color: gray !important;
        border: none !important;
      }
      .btn-col{width:2.375em;}
    </style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Main Container -->
    <div class="container-fluid">

      <!-- Modes -->
      <header>
        <div class="row">
          <div class="col-xs-6">
            <a data-mode="restore" id="btn-mode" href="#">Enter <span id="lbl-mode">Restore</span> Mode</a>
          </div>
          <div class="col-xs-6 text-right">
            Total Time: <span id="tally"></span>
          </div>
        </div>
        <div class="row">
          <div class=" col-xs-12 text-right">
            Server Time: <span id="srvtime"></span>
          </div>
        </div>
      </header>

      <!-- Input Field -->
      <div class="row">
        <form id="form-new">
          <div class="col-xs-10">
            <input id="name" name="name" type="text" class="form-control" placeholder="Enter a new task..." required>
          </div>
          <div class="col-xs-2">
            <button type="submit" class="btn btn-block btn-success"><?= icon('plus');?></button>
          </div>
        </form>
      </div>

      <hr>

      <!-- Time Table-->
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="col-xs-7">Task</th>
            <th class="col-xs-1">Start</th>
            <th class="col-xs-1">End</th>
            <th class="col-xs-1">Time</th>
            <th colspan="2" class="col-xs-2">Controls</th>
          </tr>
        </thead>
        <tbody id="log"></tbody>
      </table>

      <!-- End Container -->
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- App Scripts -->
    <script src="./moment-with-locales.js"></script>
    <script src="./time.tracker.js"></script>
  </body>
</html>
