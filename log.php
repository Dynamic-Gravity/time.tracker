<?php
include('./functions.php');
$json = file_get_contents('./data.json');
$data = json_decode($json, true);

if(is_array($data)){
  krsort($data);
}

switch($_GET['mode']){

  case "remove":
    taskRemove($data);
  break;

  case "status":
    taskActivate($data);
  break;

  case "stop":
    taskStop($data);
  break;

  case "new":
    taskNew($data);
  break;

  case "build":
    taskBuild($data);
  break;

  case "restore":
    taskRestore($data);
  break;

  case "tally" :
    taskTally($data);
  break;
}
?>
