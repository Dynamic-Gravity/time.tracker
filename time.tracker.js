"use strict";

function tally(){
  $('#tally').load('./log.php?mode=tally');
}

//displays the current time of the server
function showServerTime(){
  setInterval(function(){
    var now = moment().format('LTS');
    $('#srvtime').text(now);
  }, 1000); //every second
}

function build(mode){
  $('#log').load('./log.php?mode='+mode);
  tally();
}
//loaded when the document is ready
$(document).ready(function () {
  build('build');
  showServerTime();

  //set refresh interval
  setInterval(function(){
    var mode = $('#btn-mode').data('mode');
    if(mode == 'restore'){
      build('build');
    } else {
      build('restore');
    }

  }, 30000); //every 30 seconds



  //switches mode between active and restore
  $('#btn-mode').on('click', function(e){
    //prevent form page refresh
    e.preventDefault();

    var mode= $(this).data('mode');

    if(mode == 'restore'){
      build('restore');
      $('#lbl-mode').html('Live');
      $(this).data('mode', 'live');
    } else {
      build('build');
      $('#lbl-mode').html('Restore');
      $(this).data('mode', 'restore');
    }
  });


  //New task
  $('#form-new').submit(function(e){
    //prevent form page refresh
    e.preventDefault();

    var form = $(this);
    var data = form.serialize();


    $.ajax({
      url: './log.php?mode=new',
      data: data,
      success: function(){
        build('build');
      }
    });
  });

  //stop task
  $('#log').on('click', '.btn-stop', function(){
    var id = $(this).data('id');
    $.ajax({
      url: './log.php?mode=stop&id='+id,
      success: function(){
        build('build');
      }
    });
  });

  //Remove task
  $('#log').on('click', '.btn-remove', function(){
    var id = $(this).data('id');
    $.ajax({
      url: './log.php?mode=remove&id='+id,
      success: function(){
        build('build');
      }
    });
  });

  //Restore task
  $('#log').on('click', '.btn-restore', function(){
    var id = $(this).data('id');
    $.ajax({
      url: './log.php?mode=status&id='+id,
      success: function(){
        build('restore');
      }
    });
  });

  //populates table with tasks
  $('#log').load('./log.php?mode=build');
  $('#tally').load('./log.php?mode=tally');

});
