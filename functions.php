<?php
#used for error checking, comment to disable
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

###############################
# Basic Functions
###############################

# used for shortening font awesome icon tags
function icon($iconCode) {
  $i = '<i class="fa fa-'.$iconCode.'"></i>';
  return $i;
}

#pretify the date
function date_nice($timestamp) {
  $d = date('M j Y g:i A', $timestamp);
  return $d;
}

#tally active tasks' times together
function time_nice($seconds) {
  #round down
  $hours = floor($seconds / 3600);
  $minutes = round(($seconds % 3600) / 60);
  $tally; //to be used next
  //fix for having no tasks that are greater than 0 hours
  $tally = $hours.' hrs : '.$minutes.' mins';
  return $tally;
}

#save data to the json file
function save($data){
  $json = json_encode($data);
  $file = fopen("./data.json", "w");
  fwrite($file, $json);
}

###############################
# Switch-Case Functions
###############################

# Marks a Tasks's status to 2, removing it from active tasks list.
# Status of 2.
function taskRemove($data){
  $id = $_GET['id'];
  $data[$id]['status'] = 2;
  save($data);
}

# Activates a task.
# Status of 1
function taskActivate($data){
  $id = $_GET['id'];
  $data[$id]['status'] = 1;
  save($data);
}

# Captures the end time and puts that into into the tasks date_field index.
function taskStop($data){
  $id = $_GET['id'];
  $data[$id]['date_end'] = time();
  save($data);
}

# Creates a new task with defaults to be added to data.json
function taskNew($data){
  $time = time();
  $data[$time]['id'] = $time;
  $data[$time]['name'] = htmlspecialchars($_GET['name']);
  $data[$time]['date_start'] = $time;
  $data[$time]['date_end'] = '';
  $data[$time]['status'] = 1;
  save($data);
}

# Builds the tasks list where status is 1.
# The unique key which identifies each task is its start time.
function taskBuild($data){
  if(is_array($data)){
    foreach($data as $task){
      #only get active tasks
      if($task['status'] == 1){
      #breaking here saves us from having to put echos everywhere.
    ?>
    <tr>
      <td><?= htmlspecialchars($task['name']); ?></td>
      <td><?= date_nice($task['date_start']); ?></td>
      <td><?php if($task['date_end'] != ""){ echo date_nice($task['date_end']); } ?>
      </td>
      <td>
        <?php #check for tasks with no end time
        if($task['date_end'] == ""){
          echo time_nice(time() - $task['date_start']);
        } else { echo time_nice($task['date_end'] - $task['date_start']); }
        ?>
      </td>
      <td class="btn-col"><button data-id="<?= $task['id'];?>" class="btn btn-primary btn-stop" <?=($task['date_end'] != '')?'disabled':'';?> ><?= icon('stop');?></button></td>
      <td class="btn-col"><button data-id="<?= $task['id'];?>" class="btn btn-danger btn-remove"><?= icon('times');?></button></td>
    </tr>
      <?php #resume
      }
    }
  }
}

# Builds list of inactive tasks. Tasks where the status is 2.
function taskRestore($data){
  if(is_array($data)){
    foreach($data as $task){
      #only get inactive tasks
      if($task['status'] == 2){
    ?>
    <tr>
      <td><?= htmlspecialchars($task['name']); ?></td>
      <td><?= date_nice($task['date_start']); ?></td>
      <td><?php if($task['date_end'] != ""){ echo date_nice($task['date_end']); }?>
      </td>
      <td>
        <?php
        if($task['date_end'] == ""){
          echo time_nice(time() - $task['date_start']);
        } else { echo time_nice($task['date_end'] - $task['date_start']); }
        ?>
      </td>
      <td colspan="2" class="btn-col"><button data-id="<?= $task['id'];?>" class="btn btn-info btn-restore"><?=icon('refresh');?></button></td>
    </tr>
      <?php
      }
    }
  }
}

# Tallys up the aggregate of active tasks duration.
function taskTally($data){
  $count = 0;
  if(is_array($data)){
    #active tasks only
    foreach($data as $task){
      if($task['status'] == 1){
        #check for empty end times
        if($task['date_end'] == ""){
          $task['date_end'] = time();
        }
        $count += $task['date_end'] - $task['date_start'];
      }
    }
  }
  echo time_nice($count);
}

?>
